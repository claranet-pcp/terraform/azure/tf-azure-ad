/* Environment variables */
variable "customer" {
  description = "The first portion of the interpolated name for resources created by this module"
  type = "string"
  default = "cust"
}

variable "envtype" {
  description = "The second portion of the interpolated name for resources created by this module"
  type = "string"
  default = "dev"
}

variable "azure_location" {
  description = "The region in which to create resources"
  type = "string"
  default = "North Europe"
}

variable "azure_location_short" {
  description = "The short name for the region in which to create resources"
  type = "string"
  default = "ne"
}

/* Remote state variables */
variable "network_remote_state_access_key" {
  description = "The remote state bucket access key"
  type = "string"
}

variable "subnet_index" {
  description = "This is a manual index used when selecting the subnet from the remote state"
  type = "string"
}

/* Active Directory Domain variables */
variable "admin_username" {
  description = "The username of the domain admin account"
  type = "string"
}

variable "admin_password" {
  description = "The password of the account specified in 'admin_username'"
  type = "string"
}

variable "ad_domain" {
  description = "The FQDN of the Active Directory domain to join"
  type = "string"
}

variable "tier" {
  description = "The value of the 'Service' tag"
  type = "string"
  default = "db"
}

variable "licence" {
  description = "The value for the 'Licence' tag on resources created by this module"
  type = "string"
}

variable "PDC_instances" {
  description = "The count of principal domain controllers to create"
  type = "string"
  default = 1
}

variable "BDC_instances" {
  description = "The count of replica domain controllers to create"
  type = "string"
  default = 1
}
