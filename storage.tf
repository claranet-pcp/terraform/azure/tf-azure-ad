resource "azurerm_storage_account" "adstorage" {
  name                = "${var.customer}${var.azure_location_short}ad"
  resource_group_name = "${azurerm_resource_group.AD_group.name}"
  location            = "${var.azure_location}"
  account_type        = "Standard_LRS"

  tags {
    environment = "${var.envtype}"
  }
}

resource "azurerm_storage_container" "scripts" {
  name                  = "scripts"
  resource_group_name   = "${azurerm_resource_group.AD_group.name}"
  storage_account_name  = "${azurerm_storage_account.adstorage.name}"
  container_access_type = "private"
}

resource "azurerm_storage_blob" "pdcscript" {
  name                   = "DeployPDC.ps1"
  source                 = "${path.module}\\scripts\\DeployPDC.ps1"
  resource_group_name    = "${azurerm_resource_group.AD_group.name}"
  storage_account_name   = "${azurerm_storage_account.adstorage.name}"
  storage_container_name = "${azurerm_storage_container.scripts.name}"
  type                   = "block"
  size                   = "512"
}

resource "azurerm_storage_blob" "bdcscript" {
  name                   = "DeployBDC.ps1"
  source                 = "${path.module}\\scripts\\DeployBDC.ps1"
  resource_group_name    = "${azurerm_resource_group.AD_group.name}"
  storage_account_name   = "${azurerm_storage_account.adstorage.name}"
  storage_container_name = "${azurerm_storage_container.scripts.name}"
  type                   = "block"
  size                   = "512"
}
