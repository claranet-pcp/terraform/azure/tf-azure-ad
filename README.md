# tf-azure-ad

Create 2 x AD servers on Azure

## Usage
```
module "AD" {
  source             = "../localmodules//tf-azure-AD"
  customer           = "${var.customer}"
  envtype            = "${var.envtype}"
  azure_location     = "${var.azure_location}"
  vhd_container_path = "${azurerm_storage_account.infrasa.primary_blob_endpoint}${azurerm_storage_container.VHD_Container.name}"
  admin_username     = "${var.admin_username}"
  admin_password     = "${var.admin_password}"
  ad_domain          = "${var.ad_domain}"
  nat_subnets        = "${module.virtual_network.nat_subnets}"
  tier               = "${var.tier}"
  licence            = "${var.licence}"
    PDC_instances  = "${var.PDC_instances}"
  BDC_instances  = "${var.BDC_instances}"
  azure_location_short = "test"
}
```


## Variables
```
* Customer - Customer Name
* envtype - Prod / Non Prod
* location - Azure Region
* vhd_container_path - Azure container to hold VHD images
* instance_number - Instance Counter
* admin_username  - Domain admin username (to be created)
* admin_password - Domain admin password (to be created)
* ad_domain - AD Domain name
* nat_subnets -  Subnet containing the DC's
* tier  = Tag for VM's
* licence  = Included / Purchased
* PDC_instances - number of primary domain controllers (should be set to 1 in main region an 0 in all other regions)
* BDC_instances - number of extra domain controllers in region
* azure_location_short - short name for region - used in computer names due to 16 character limit.
```

## Output
```
 ad_group_name - resource group of AD servers
```

```
The scripts are pushed to Azure blob via the resources below :

resource "azurerm_storage_account" "infrasa" {
  name                = "${var.customer}${var.envtype}infrasa"
  resource_group_name = "${azurerm_resource_group.Infrastructure.name}"
  location            = "${var.azure_location}"
  account_type        = "Standard_LRS"

  tags {
    environment = "${var.envtype}"
  }
}

resource "azurerm_storage_container" "VHD_Container" {
  name                  = "${var.customer}vhds"
  resource_group_name   = "${var.customer}Infrastructure"
  storage_account_name  = "${azurerm_storage_account.infrasa.name}"
  container_access_type = "private"
}

resource "azurerm_storage_container" "scripts" {
  name                  = "scripts"
  resource_group_name   = "${var.customer}Infrastructure"
  storage_account_name  = "${azurerm_storage_account.infrasa.name}"
  container_access_type = "private"

```
```
*******************  Azure DNS needs to be updated to point to the DC's after creations ************************
```

