resource "azurerm_resource_group" "AD_group" {
  name     = "${var.customer}-${var.envtype}-${var.azure_location}-AD"
  location = "${var.azure_location}"

  tags {
    environment = "${var.envtype}"
    service     = "${var.tier}"
  }
}

resource "azurerm_availability_set" "AD-AvailabilitySet" {
  name                = "${var.customer}-${var.envtype}-${var.azure_location}-AD-ASet"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.AD_group.name}"
  managed             = "true"

  tags {
    environment = "${var.envtype}"
    service     = "${var.tier}"
  }
}
