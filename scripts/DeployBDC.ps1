﻿$domain = $args[0]
$adminuser= $args[1]
$adminpass = $args[2]
$DCIP = $args[3]




Start-Transcript -Path C:\Scripts\AD_Dsc.Log

# Setting the NLA information to Disabled
(Get-WmiObject -class "Win32_TSGeneralSetting" -Namespace root\cimv2\terminalservices -ComputerName localhost -Filter "TerminalName='RDP-tcp'").SetUserAuthenticationRequired(0)



#SET DNS
# Retrieve the network adapter that you want to configure
$adapter = Get-NetAdapter | ? {$_.Status -eq "up"}
$adapter | Set-DnsClientServerAddress -ServerAddresses $DCIP


########################################
# PowerShell Script to Install Domain Controllers #
########################################
$secpasswd = ConvertTo-SecureString $adminpass -AsPlainText -Force
$DomainCreds = New-Object System.Management.Automation.PSCredential ("$Domain\$adminuser", $secpasswd )

$featureLogPath = “c:\poshlog\featurelog.txt” 
New-Item $featureLogPath -ItemType file -Force 
$addsTools = “RSAT-AD-Tools” 
Add-WindowsFeature $addsTools 
Get-WindowsFeature | Where installed >>$featureLogPath
start-job -Name addFeature -ScriptBlock { 
Add-WindowsFeature -Name “ad-domain-services” -IncludeAllSubFeature -IncludeManagementTools 
Add-WindowsFeature -Name “gpmc” -IncludeAllSubFeature -IncludeManagementTools }
Wait-Job -Name addFeature 
Get-WindowsFeature | Where installed >>$featureLogPath
Import-Module ADDSDeployment


#wait for domain
#Do {
#$ADCheck = $null
#$ADCheck = get-addomain $domain 
#write-host "....waiting for domain"
#Start-Sleep -s 5
#}
#While ($ADcheck -eq $null)

start-sleep -s 360


#install Domain Contr0ller
Install-ADDSDomainController `
-credential $DomainCreds `
-NoGlobalCatalog:$false `
-SafeModeAdministratorPassword $secpasswd `
-InstallDns:$True `
-CreateDnsDelegation:$false `
-CriticalReplicationOnly:$false `
-DatabasePath "C:\Windows\NTDS" `
-LogPath "C:\Windows\NTDS" `
-SysvolPath "C:\Windows\SYSVOL" `
-DomainName $domain `
-NoRebootOnCompletion:$false `
-Force:$true





Write-Host "BDC Configuration Applied - Rebooting"

Stop-Transcript