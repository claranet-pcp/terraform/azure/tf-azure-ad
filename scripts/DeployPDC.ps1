﻿$domain = $args[0]
$adminpass = $args[1]

Start-Transcript -Path C:\Scripts\AD_Dsc.Log

# Setting the NLA information to Disabled
(Get-WmiObject -class "Win32_TSGeneralSetting" -Namespace root\cimv2\terminalservices -ComputerName localhost -Filter "TerminalName='RDP-tcp'").SetUserAuthenticationRequired(0)


$secpasswd = ConvertTo-SecureString $adminpass -AsPlainText -Force

#set DNS address 
# Retrieve the network adapter that you want to configure
$adapter = Get-NetAdapter | ? {$_.Status -eq "up"}
$adapter | Set-DnsClientServerAddress -ServerAddresses 127.0.0.1
#install features 
$featureLogPath = “c:\poshlog\featurelog.txt” 
New-Item $featureLogPath -ItemType file -Force 
$addsTools = “RSAT-AD-Tools” 
Add-WindowsFeature $addsTools 
Get-WindowsFeature | Where installed >>$featureLogPath 



#Install AD DS, DNS and GPMC 
$featureLogPath = “c:\poshlog\featurelog.txt” 
start-job -Name addFeature -ScriptBlock { 
Add-WindowsFeature -Name “ad-domain-services” -IncludeAllSubFeature -IncludeManagementTools 
Add-WindowsFeature -Name “dns” -IncludeAllSubFeature -IncludeManagementTools 
Add-WindowsFeature -Name “gpmc” -IncludeAllSubFeature -IncludeManagementTools } 
Wait-Job -Name addFeature 
Get-WindowsFeature | Where installed >>$featureLogPath

$domainname = $domain
$netbiosName = $domainname.split('.')[0]
Import-Module ADDSDeployment 
Install-ADDSForest -CreateDnsDelegation:$false `
-DatabasePath “C:\Windows\NTDS” `
-SafeModeAdministratorPassword $secpasswd `
-DomainName $domainname  `
-DomainNetbiosName $netbiosName `
-InstallDns:$true `
-LogPath “C:\Windows\NTDS” `
-NoRebootOnCompletion:$false `
-SysvolPath “C:\Windows\SYSVOL” `
-Force:$true

#restart the computer 
#Restart-Computer




Write-Host "DSC Configuration Applied - Rebooting"

Stop-Transcript

