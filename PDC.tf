resource "azurerm_network_interface" "DC_network_interface" {
  count               = "${var.PDC_instances}"
  name                = "${var.customer}-${var.envtype}-${var.azure_location}-DC-win-${count.index}-interface"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.AD_group.name}"

  //network_security_group_id = "${azurerm_network_security_group.windows_bastion_sg.id}"

  ip_configuration {
    name                          = "${var.customer}-${var.envtype}-${var.azure_location}-DC-win-${count.index}-ipconfig"
    subnet_id                     = "${element(split(",",data.terraform_remote_state.network.nat_subnets),var.subnet_index)}"
    private_ip_address_allocation = "static"
    private_ip_address            = "${cidrhost(element(split(",",data.terraform_remote_state.network.nat_subnets_prefix),var.subnet_index), count.index + 10)}"
  }
  tags {
    environment = "${var.envtype}"
    service     = "${var.tier}"
  }
}

resource "azurerm_virtual_machine" "DC_host" {
  count                 = "${var.PDC_instances}"
  name                  = "${var.customer}-${var.envtype}-${var.azure_location}-DC-${count.index}"
  location              = "${var.azure_location}"
  resource_group_name   = "${azurerm_resource_group.AD_group.name}"
  network_interface_ids = ["${element(azurerm_network_interface.DC_network_interface.*.id, count.index)}"]
  vm_size               = "Standard_A2"                                                                    # Never A0!!! Maybe switch to F2 -only in certain regions
  availability_set_id   = "${azurerm_availability_set.AD-AvailabilitySet.id}"

  storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${var.customer}-${var.envtype}-${var.azure_location}-DC-win-${count.index}-OS.vhd"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${var.azure_location_short}-DC-${count.index}"
    admin_username = "${var.admin_username}"
    admin_password = "${var.admin_password}"
  }

  os_profile_windows_config {
    provision_vm_agent        = true
    enable_automatic_upgrades = true
  }

  tags {
    environment = "${var.envtype}"
    service     = "${var.tier}"
    licence     = "${var.licence}"
  }
}

resource "azurerm_virtual_machine_extension" "pdc_ext" {
  count                = "${var.PDC_instances}"
  name                 = "${var.customer}-${var.envtype}-${var.azure_location}-PDC"
  location             = "${var.azure_location}"
  depends_on           = ["azurerm_virtual_machine.DC_host"]
  resource_group_name  = "${azurerm_resource_group.AD_group.name}"
  virtual_machine_name = "${var.customer}-${var.envtype}-${var.azure_location}-DC-${count.index}"
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.7"

  settings = <<SETTINGS
    {
        "fileUris": [
           "https://${azurerm_storage_account.adstorage.name}.blob.core.windows.net/scripts/DeployPDC.ps1"
       ]
    }
    SETTINGS

  protected_settings = <<SETTINGS
{
    "commandToExecute": "powershell.exe -ExecutionPolicy Unrestricted -File DeployPDC.ps1 ${var.ad_domain} ${var.admin_password}",
    "storageAccountName": "${azurerm_storage_account.adstorage.name}",
    "storageAccountKey" : "${azurerm_storage_account.adstorage.primary_access_key}"
}
  SETTINGS
}

resource "azurerm_virtual_machine_extension" "Malware_Protection_Extension_PDC" {
  name                 = "IaaSAntimalware"
  count                = "${var.PDC_instances}"
  location             = "${var.azure_location}"
  resource_group_name  = "${azurerm_resource_group.AD_group.name}"
  virtual_machine_name = "${var.customer}-${var.envtype}-${var.azure_location}-DC-${count.index}"
  publisher            = "Microsoft.Azure.Security"
  type                 = "IaaSAntimalware"
  type_handler_version = "1.5"

  settings = <<SETTINGS
    { "AntimalwareEnabled": true, 
    "RealtimeProtectionEnabled": true, 
    "ScheduledScanSettings": { 
        "isEnabled": true, 
        "day": "1", 
        "time": "120", 
        "scanType": "Full" }, 
        "Exclusions": { 
            "Extensions": ".mdf;.ldf", 
            "Paths": "c:\\windows;c:\\windows\\system32", 
            "Processes": "taskmgr.exe;notepad.exe" } 
            }
  SETTINGS
}
