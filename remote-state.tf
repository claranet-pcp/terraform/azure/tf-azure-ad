data "terraform_remote_state" "network" {
  backend = "azure"

  config {
    storage_account_name = "bashton"
    container_name       = "${var.customer}-${var.envtype}-tfstate"
    key                  = "network-${var.azure_location}.tfstate"
    access_key           = "${var.network_remote_state_access_key}"
  }
}
